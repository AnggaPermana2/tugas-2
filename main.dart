import 'package:flutter/material.dart';
void main() {
  runApp(
    MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text('Contoh Layout'),
          backgroundColor: Colors.red[600],
        ),
        body: App(),
      ),
    ),
  );
}

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container();
  }
}
class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.blue,
      margin: EdgeInsets.only(top: 20, right: 50),
      child: Container(
        color: Colors.green,
         margin: EdgeInsets.symmetric(vertical: 200),
        child: Container(
          color: Colors.yellow,
          margin: EdgeInsets.fromLTRB(0, 20, 200, 20),
        ),
      ),
    );
  }
}
class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          // Row 1
          Row(
            children: <Widget>[
              Container(
                color: Colors.blue, height: 40, width: 40, child: Text('1')),
              Container(
                color: Colors.blue, height: 40, width: 40, child: Text('2')),
              Container(
                color: Colors.blue, height: 40, width: 40, child: Text('3')),
            ],
          ),
          Row(
            children: <Widget>[
              Container(
                color: Colors.blue, height: 40, width: 40, child: Text('1')),
              Expanded(
                  child: Container(
                    color: Colors.green,
                    height: 40,
                    width: 40,
                    child: Text('2'))),
              Container(
                color: Colors.blue, height: 40, width: 40, child: Text('3')),
            ],
          ),
                    Container(
              height: 100,
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  Container(
                    color: Colors.blue,
                    height: 40,
                    width: 40,
                    child: Text('1')),
                  Expanded(
                      child: Container(
                        color: Colors.green,
                        height: 40,
                        width: 40,
                        child: Text('2'))),
                  Container(
                    color: Colors.blue,
                    height: 40,
                    width: 40,
                    child: Text('3')),
                ],
              )),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              Container(
                color: Colors.blue, height: 40, width: 40, child: Text('1')),
              Container(
                color: Colors.blue, height: 40, width: 40, child: Text('1')),
              Container(
                color: Colors.blue, height: 40, width: 40, child: Text('3')),
            ],
          )
        ]);
  }
}
